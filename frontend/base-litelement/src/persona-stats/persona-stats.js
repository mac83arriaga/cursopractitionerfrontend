import {LitElement,html} from 'lit-element';


class PersonaStats extends LitElement{

    static get properties(){
        //las propiedades se devuelven en un object
        return {
            people: {type: Array},
            maxYear: {type: Number}//MAC - filtro años      
        };
    }

    constructor(){
        super();

        this.people = [];
        this.maxYear = 0;//MAC - filtro años      
                
    }

    updated(changedProperties){
        //console.log("updated");

        if(changedProperties.has("people")){
            console.log("ha cambiado la propiedad de people en persona-stats");
            
            let peopleStats=this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail: {
                    peopleStats: peopleStats
                }
            }
                
            ));
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        this.calculateMaxYear();//MAC - filtro años      
        peopleStats.maxYear = this.maxYear;//MAC - filtro años      
        return peopleStats;
    }

    //MAC - filtro años      
    calculateMaxYear(){
        this.maxYear=0;
        for(var i=0;i<this.people.length;i++){
            //console.log("comparando:"+this.people[i].yearsInCompany+" con "+this.maxYear);
            if(this.people[i].yearsInCompany>this.maxYear){
                this.maxYear=this.people[i].yearsInCompany;
            }
            console.log("MAXIMOS AÑOS:"+this.maxYear);
        }
    
    
        //otra manera de hacerlo
        /*
            people.forEach(person => {
                if(person.yearsInCompany > maxYearsInCompany){
                    maxYearsInCompany = person.yearsInCompany
                }
                
            }
                );
        */ 
    
    
    }



    //no hay render ya que no va a mostrar nada. se trata de un webcomponent de calculo solamente


}

customElements.define('persona-stats',PersonaStats);