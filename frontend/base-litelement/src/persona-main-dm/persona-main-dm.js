import {LitElement,html} from 'lit-element';


class PersonaMainDm extends LitElement{

    static get properties(){
        //las propiedades se devuelven en un object
        return {
            people: {type: Array},
        };
    }

    constructor(){
        super();


        //base de datos de people
        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile:"Lorem ipsum dolor sit amet.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Ellen Ripley"
                }
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile:"Lorem ipsum.",
                photo:{
                    src: "./img/photo.png",
                    alt:"Bruce Banner"
                }
            },
            {
                name: "Eowyn",
                yearsInCompany: 5,
                profile:"Lorem ipsum dolor sit amet. asiodhaso.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Eowyn"
                }
            },

            {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile:"Lorem ip.",
                photo:{
                    src: "./img/photo.png",
                    alt:"Turanga Leela"
                }
            },
            {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile:"Lore.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Tyrion Lannister"
                }
            }

        ];   
                
    }


    updated(changedProperties){
        console.log("MAC -----------------------------------------------updated BD");

        if(changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad people en persona-main-dm");

            for(var i=0;i<this.people.length;i++){
              console.log(this.people[i].name+" "+this.people[i].yearsInCompany);
            }

            this.dispatchEvent(new CustomEvent("updated-people-dm",            
            {
                detail:{
                    people: this.people
                }
            }

            )
            );
        }

    }

}

customElements.define('persona-main-dm',PersonaMainDm);