import {LitElement,html, css} from 'lit-element';

class TestBootstrap extends LitElement{

    static get styles(){
        return css`
            .redbg{
                background-color: red;
            }        
            
            .greenbg{
                background-color: green;
            }        

            .bluebg{
                background-color: blue;
            }        
            
            .graybg{
                background-color: gray;
            }        

        `;

    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        
        <div class="row graybg">
            <h3>Hola booot!</h3>
            <div class="col-2 offset-1 redbg">Col 1</div>
            <div class="col-3 offset-2 greenbg">Col 2</div>
            <div class="col-4 bluebg">Col 3</div>
        </div>


        `;

    }

}

customElements.define('test-bootstrap',TestBootstrap);