import {LitElement,html} from 'lit-element';

class TestApi extends LitElement{


    static get properties(){
        return{
            movies:{type:Array}
        };
    }

    constructor(){
        super();

        this.movies=[];
        this.getMovieData();
    }

    render(){
        return html`
            <h1>STAR WARS</h1>
            ${this.movies.map(
                movie => html`<div>La película ${movie.title} fue dirigida por ${movie.director}</div>`
            )}         
        `;

    }

    getMovieData(){
        console.log("Obteniendo datos de las peliculas");

        let xhr=new XMLHttpRequest();

        //onload es una propiedad que se dispara como un evento cuando se ha terminado la peticion
        xhr.onload = () => {
            //si ha ido bien la consulta
            if(xhr.status === 200){
                console.log("peticion completada correctamente");
                
                let APIResponse = JSON.parse(xhr.responseText);

                console.log(xhr.responseText);

                this.movies=APIResponse.results;

            }
        }
    
        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();
        console.log("Fin getmoviedata");

    }

}

customElements.define('test-api',TestApi);
