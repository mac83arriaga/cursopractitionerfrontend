import {LitElement,html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-main-dm/persona-main-dm.js'


class PersonaMain extends LitElement{

    static get properties(){
        //las propiedades se devuelven en un object
        return {
            people: {type: Array},                 
            filtedPeople: {type: Array},           //MAC - filtro años      
            yearsFilter: {type: Number},//MAC - filtro años      
            showPersonForm:{type:Boolean}
        };
    }

    constructor(){
        super();

        this.people=[];
/*        
        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile:"Lorem ipsum dolor sit amet.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Ellen Ripley"
                }
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile:"Lorem ipsum.",
                photo:{
                    src: "./img/photo.png",
                    alt:"Bruce Banner"
                }
            },
            {
                name: "Eowyn",
                yearsInCompany: 5,
                profile:"Lorem ipsum dolor sit amet. asiodhaso.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Eowyn"
                }
            },

            {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile:"Lorem ip.",
                photo:{
                    src: "./img/photo.png",
                    alt:"Turanga Leela"
                }
            },
            {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile:"Lore.",
                photo:{
                    src: "./img/photo.png",
                    alt: "Tyrion Lannister"
                }
            }

        ];   
*/
        this.filtedPeople = this.people; //MAC - filtro años      
        this.yearsFilter=1;
        //inicializamos ocultando el formulario
        this.showPersonForm=false;             
    }

    //@change es el onchange mientas que @input cubre todo
    render(){

        /*
            Se puede hacer tambien
            ${this.people.filter(person => person.yearsInCompany <= this. maxYearsIncCompanyFilter).map(....)
            }
    
        */


        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">            

            <persona-main-dm id="dm" @updated-people-dm="${this.updatedPeopleDM}"></persona-main-dm>

            <h2 class="text-center">Personas</h2>
                <div class="row"  id="peopleList">
                    <div class="row row-cols-1 row-cols-sm-4">                    
                    ${this.filtedPeople.map(
                    person => html`<persona-ficha-listado 
                        fname="${person.name}"
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                        ></persona-ficha-listado>`
                     )}
                    </div>                    
                </div> 
                <div class="row">
                    <persona-form id="personForm" class="d-none border rounded border-primary" @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"                    
                    ></persona-form>
                </div>           
        `;

    }
    //////////////////////////////////////////DM
    updatedPeopleDM(e){
        console.log("updatedPeopleDM");
        this.people=e.detail.people;
    }

    personFormStore(e){
        console.log("PersonaFormStore");
        console.log("Se va a almacenar una persona");

        console.log("El Name de la persona es " + e.detail.name);
        console.log("El Profile de la persona es " + e.detail.profile);
        console.log("El yearsInCompany de la persona es " + e.detail.yearsInCompany);

        let peopleAux=[];

        if(e.detail.editingPerson === true){
            console.log("Se va a a actualizar la persona con nombre "+e.detail.person.name);

            peopleAux = this.people.map(
                person => person.name === e.detail.person.name
                 ? person = e.detail.person : person);

                 //añadido para que detecte cambio cuando se edite //MAC - filtro años      
                 //peopleAux= [...this.people];        
        }
        else{
            peopleAux= [...this.people,e.detail.person];   
        }

        console.log("Fin proceso guardado");

        //grabamos en DM
        this.shadowRoot.getElementById("dm").people=peopleAux;

        this.showPersonForm=false;

    }

    deletePerson(e){
        console.log("delete person en persona-main "+ e.detail.name);
        let people = this.people.filter(
            person => person.name != e.detail.name
        );

        //grabamos en DM
        this.shadowRoot.getElementById("dm").people=people;
    }

    /////////////////////////////////////////FIN DM


    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }

        }

        //MAC - filtro años      
        if(changedProperties.has("yearsFilter")){
            console.log("MAC - ha cambiado el filtro");                         
            this.calculateFiltedPeople();
        }

        if(changedProperties.has("people")){
            console.log("ha cambiado el tamaño de people");                          
            this.calculateFiltedPeople();//MAC - filtro años      
            this.dispatchEvent(new CustomEvent("updated-people",            
            {
                detail:{
                    people: this.people,                    
                }
            }

            )
            );
            
        }



    }

    //MAC - filtro años      
    calculateFiltedPeople(){
        let filtP=[];
        for(var i=0;i<this.people.length;i++){
            if(parseInt(this.people[i].yearsInCompany,10)<=this.yearsFilter)
                filtP.push(this.people[i]);
        }

        this.filtedPeople=filtP;
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("mostrando listado de personas");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("mostrando formulario de persona");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }


    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm=false;
    }

    personFormStore_old(e){
        console.log("PersonaFormStore");
        console.log("Se va a almacenar una persona");

        console.log("El Name de la persona es " + e.detail.name);
        console.log("El Profile de la persona es " + e.detail.profile);
        console.log("El yearsInCompany de la persona es " + e.detail.yearsInCompany);

        if(e.detail.editingPerson === true){
            console.log("Se va a a actualizar la persona con nombre "+e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                 ? person = e.detail.person : person);

                 //añadido para que detecte cambio cuando se edite //MAC - filtro años      
                 this.people= [...this.people];        
        }
        else{
            this.people= [...this.people,e.detail.person];   
        }



            /* no detecta que se cambie el array, hay que cambiarlo entero para que lo detecte
            puntero distinto
 
            let indexOfPerson = this.people.findIndex(
                person => person.name == e.detail.person.name
            );

 
            if(indexOfPerson >= 0){
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;

            }
            
        }else{
            console.log("Se va a guardar una persona nueva");
            this.people.push(e.detail.person);
        }
*/




        console.log("Fin proceso guardado");

        this.showPersonForm=false;

    }

    deletePerson_old(e){
        console.log("delete person en persona-main "+ e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e){
        console.log("InfoPerson en persona-main");
        console.log("Se ha pedido mas informacion de la persona "+ e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let personE = {};
        personE.name = chosenPerson[0].name;
        personE.profile = chosenPerson[0].profile;
        personE.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = personE;
        this.shadowRoot.getElementById("personForm").editingPerson = true;

        this.showPersonForm=true;        
    }


}

customElements.define('persona-main',PersonaMain);