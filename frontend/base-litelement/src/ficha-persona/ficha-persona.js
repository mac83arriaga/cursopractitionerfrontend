import {LitElement,html} from 'lit-element';


//  <ficha-persona name="Otro nombre" yearsInCompany="10"></ficha-persona>
class FichaPersona extends LitElement{

    static get properties(){
        //las propiedades se devuelven en un object
        return {
          name: {type: String},
          yearsInCompany: {type: Number},
          personInfo: {type:String}
        };
    }



    constructor(){
        super();

        this.name="Prueba nombre";
        this.yearsInCompany=12;
                
    }

    updatePersonInfo(){
        if(this.yearsInCompany >= 7){
            this.personInfo="lead";
        }
        else if(this.yearsInCompany >=5){
            this.personInfo="senior";
        }
        else if(this.yearsInCompany >=3){
            this.personInfo="team";
        }else{
            this.personInfo="junior";
        }
    }


    updated(changedProperties){
        console.log("updated");

        changedProperties.forEach((oldValue,propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era "+ oldValue);
            console.log(`${propName} changed. oldValue: ${oldValue}`);

        });

        if(changedProperties.has("name")){
            console.log("probando "+changedProperties.get("name") + ",nuevo es "+ this.name);
        }

        if(changedProperties.has("yearsInCompany")){
            console.log("probando "+changedProperties.get("yearsInCompany") + ",nuevo es "+ this.yearsInCompany);
            this.updatePersonInfo();
        }

        
    }


    //@change es el onchange mientas que @input cubre todo
    render(){
        return html`
            <div>
                <label>Nombre completo:</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa:</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>                
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>                
                <br />
            </div>
        `;

    }

    updateName(e){//cambio en el name
        console.log("updateName");
        //actualizamos propiedad de clase
        this.name= e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYears");
        //actualizamos propiedad de clase
        this.yearsInCompany= e.target.value;
    }



}

customElements.define('ficha-persona',FichaPersona);