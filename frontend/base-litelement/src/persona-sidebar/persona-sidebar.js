import {LitElement,html} from 'lit-element';


class PersonaSidebar extends LitElement{

    static get properties(){
        //las propiedades se devuelven en un object
        return {
            peopleStats:{type:Object},
            yearsFilter:{type:Number}//MAC - filtro años      
        };
    }

    constructor(){
        super();
        this.peopleStats={};
        this.yearsFilter=1;                //MAC - filtro años      
    }


    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>

                    <div>
                        <label for="customRange3" class="form-label">Años en la empresa ${this.yearsFilter}</label>
                        <input type="range" class="form-range" min="1" max="${this.peopleStats.maxYear}"  step="0.5" value="${this.yearsFilter}" id="customRange" @input="${this.updateFilter}">
                    </div>

                    <div class="mt-5">
                        <button class="w-100 btn bg-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                </section>
            </aside>

        `;

    }

    updated(changedProperties){
        console.log("updated");
        //MAC - filtro años      
        if(changedProperties.has("yearsFilter")){

            this.dispatchEvent(new CustomEvent("updated-filter",{
                detail: {
                    yearsFilter: this.yearsFilter
                }
            }
                
            ));
                

        }
    }
    //MAC - filtro años      
    updateFilter(e){
        console.log("updateFilter:"+e.target.value);
        this.yearsFilter=e.target.value;
    }

    newPerson(e){
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una persona");

        this.dispatchEvent(new CustomEvent("new-person",{}));
    }


}

customElements.define('persona-sidebar',PersonaSidebar);